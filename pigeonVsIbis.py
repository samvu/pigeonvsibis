# imports
import pygame
import sys
import tkinter as tk
from random import randint
from time import sleep
from math import sin
# program and variable initialisation
pygame.init()
screen = pygame.display.set_mode((800,600))
pygame.display.set_caption("Pigeon vs Ibis")
pygame.display.set_icon(pygame.image.load("images/pigeonStill.png"))
scene = 1
running = True
playerInitialised = False
started = False
selectedCharNum = 1
gibisList = []
skibisList = []
offset = 32
score = 0
# pigeon sprite loading
sprPigeonStillR = pygame.image.load("images/pigeonStill.png")
sprPigeonStillL = pygame.transform.flip(sprPigeonStillR, True, False)
sprPigeonWalk1R = pygame.image.load("images/pigeonWalk1.png")
sprPigeonWalk1L = pygame.transform.flip(sprPigeonWalk1R, True, False)
sprPigeonWalk2R = pygame.image.load("images/pigeonWalk2.png")
sprPigeonWalk2L = pygame.transform.flip(sprPigeonWalk2R, True, False)
sprPigeonFly1R = pygame.image.load("images/pigeonFly1.png")
sprPigeonFly1L = pygame.transform.flip(sprPigeonFly1R, True, False)
sprPigeonFly2R = pygame.image.load("images/pigeonFly2.png")
sprPigeonFly2L = pygame.transform.flip(sprPigeonFly2R, True, False)
sprPigeonSelected = pygame.image.load("images/pigeonSelected.png")
# lord sprite loading
sprLordStillR = pygame.image.load("images/lordStill.png")
sprLordStillL = pygame.transform.flip(sprLordStillR, True, False)
sprLordWalk1R = pygame.image.load("images/lordWalk1.png")
sprLordWalk1L = pygame.transform.flip(sprLordWalk1R, True, False)
sprLordWalk2R = pygame.image.load("images/lordWalk2.png")
sprLordWalk2L = pygame.transform.flip(sprLordWalk2R, True, False)
sprLordJumpR = pygame.image.load("images/lordJump.png")
sprLordJumpL = pygame.transform.flip(sprLordJumpR, True, False)
sprLordSelected = pygame.image.load("images/lordSelected.png")
# shredded sprite loading
sprShreddedStillR = pygame.image.load("images/shreddedStill.png")
sprShreddedStillL = pygame.transform.flip(sprShreddedStillR, True, False)
sprShreddedWalk1R = pygame.image.load("images/shreddedWalk1.png")
sprShreddedWalk1L = pygame.transform.flip(sprShreddedWalk1R, True, False)
sprShreddedWalk2R = pygame.image.load("images/shreddedWalk2.png")
sprShreddedWalk2L = pygame.transform.flip(sprShreddedWalk2R, True, False)
sprShreddedJumpR = pygame.image.load("images/shreddedJump.png")
sprShreddedJumpL = pygame.transform.flip(sprShreddedJumpR, True, False)
sprShreddedSelected = pygame.image.load("images/shreddedSelected.png")
# bearded sprite loading
sprBearded = pygame.image.load("images/bearded.png")
sprBeardedSelected = pygame.image.load("images/beardedSelected.png")
# gibis sprite loading
sprGibisStillR = pygame.image.load("images/ibisStill.png")
sprGibisStillL = pygame.transform.flip(sprGibisStillR, True, False)
sprGibisWalk1R = pygame.image.load("images/ibisWalk1.png")
sprGibisWalk1L = pygame.transform.flip(sprGibisWalk1R, True, False)
sprGibisWalk2R = pygame.image.load("images/ibisWalk2.png")
sprGibisWalk2L = pygame.transform.flip(sprGibisWalk2R, True, False)
# skibis sprite loading
sprSkibis1R = pygame.image.load("images/ibisFly1.png")
sprSkibis1L = pygame.transform.flip(sprSkibis1R, True, False)
sprSkibis2R = pygame.image.load("images/ibisFly2.png")
sprSkibis2L = pygame.transform.flip(sprSkibis2R, True, False)
# tile sprite loading
sprTile = pygame.image.load("images/grass.png")
# title sprite loading
sprTitle = pygame.image.load("images/pigeonVsIbis.png")
# player coding
class player:
	# variable creation
	def __init__(self,x,y,grav,vxl,vxr,vx,vy,spr,wset,walking,alive,jumpLimit,direction,spd,dmg):
		if selectedCharNum == 1:
			global charSpr
		global charData
		# character stat loading
		if selectedCharNum == 1:
			charData = {
				"jLimit" : 5,
				"jMax" : 5,
				"spd" : 1.5,
				"damage" : 1
			}
			charSpr = {
				"rs" : sprPigeonStillR,
				"rw1" : sprPigeonWalk1R,
				"rw2" : sprPigeonWalk2R,
				"rf1" : sprPigeonFly1R,
				"rf2" : sprPigeonFly2R,
				"ls" : sprPigeonStillL,
				"lw1" : sprPigeonWalk1L,
				"lw2" : sprPigeonWalk2L,
				"lf1" : sprPigeonFly1L,
				"lf2" : sprPigeonFly2L
			}
			self.spr = sprPigeonStillR
		if selectedCharNum == 2:
			charData = {
				"jLimit" : 1,
				"jMax" : 1,
				"spd" : 0.9,
				"damage" : 3
			}
			charSpr = {
				"rs" : sprLordStillR,
				"rw1" : sprLordWalk1R,
				"rw2" : sprLordWalk2R,
				"rf1" : sprLordJumpR,
				"rf2" : sprLordJumpR,
				"ls" : sprLordStillL,
				"lw1" : sprLordWalk1L,
				"lw2" : sprLordWalk2L,
				"lf1" : sprLordJumpL,
				"lf2" : sprLordJumpL
			}
		if selectedCharNum == 3:
			charData = {
				"jLimit" : 1,
				"jMax" : 1,
				"spd" : 15,
				"damage" : 1.5
			}
			charSpr = {
				"rs" : sprShreddedStillR,
				"rw1" : sprShreddedWalk1R,
				"rw2" : sprShreddedWalk2R,
				"rf1" : sprShreddedJumpR,
				"rf2" : sprShreddedJumpR,
				"ls" : sprShreddedStillL,
				"lw1" : sprShreddedWalk1L,
				"lw2" : sprShreddedWalk2L,
				"lf1" : sprShreddedJumpL,
				"lf2" : sprShreddedJumpL
			}
		if selectedCharNum == 4:
			charData = {
				"jLimit" : 1,
				"jMax" : 1,
				"spd" : 2,
				"damage" : 1.5
			}
			charSpr = {
				"rs" : sprBearded,
				"rw1" : sprBearded,
				"rw2" : sprBearded,
				"rf1" : sprBearded,
				"rf2" : sprBearded,
				"ls" : sprBearded,
				"lw1" : sprBearded,
				"lw2" : sprBearded,
				"lf1" : sprBearded,
				"lf2" : sprBearded
			}
		self.x = 400
		self.y = 0
		self.vxl = 0
		self.vxr = 0
		self.vx = 0
		self.vy = 0
		self.grav = 0
		self.jumps = charData["jMax"]
		self.jumpLimit = charData["jLimit"]
		self.spr = charSpr["rs"]
		self.alive = False
		self.direction = "right"
		self.spd = charData["spd"]
		self.wset = 0
		self.dmg = charData["damage"]
		self.walking = False
	# draws pigeon on the screen
	def draw(self):
		if self.alive == True and scene == 2:
			screen.blit(self.spr, (self.x,self.y))
	# platforming
	def platforming(self):
		for event in pygame.event.get():
			# to quit
			if event.type == pygame.QUIT:
				running = False
				pygame.quit()
				sys.exit()
			# keystrokes
			if event.type == pygame.KEYDOWN:
				# left/right movement
				if event.key == pygame.K_LEFT:
					self.direction = "left"
					self.vxl = self.vxl + self.spd
					self.vxr = 0
				if event.key == pygame.K_RIGHT:
					self.direction = "right"
					self.vxr = self.vxr + self.spd
					self.vxl = 0
				# jumping
				if event.key == pygame.K_UP and self.jumps <= (self.jumpLimit - 1):
					self.jumps = self.jumps + 1
					self.grav = -5
			if event.type == pygame.KEYUP:
				if event.key == pygame.K_LEFT:
					self.vxl = 0
				if event.key == pygame.K_RIGHT:
					self.vxr = 0
		# changes the player x
		self.vx = self.vxr - self.vxl
		self.x = self.x + self.vx
		# walking check:
		if self.vx == 0:
			self.walking = False
		else:
			self.walking = True
		# gravity
		self.y = self.y + self.grav
		self.grav = self.grav + 0.09
		# boundaries
		if self.x <= 0:
			self.x = 0
		if self.x >= 768:
			self.x = 768
		if self.y >= 535:
			self.jumps = 0
			self.grav = 0
			self.y = 536
		if self.y <= 0:
			self.y = 0
	# sets the sprite
	def sprSet(self):
		if self.direction == "right":
			if self.jumps == 0:
				# walk cycle set
				if self.walking == True:
					self.wset = self.wset + 0.1
					if self.wset > 3:
						self.wset = 1
				else:
					self.wset = 0
				if int(self.wset) == 0:
					self.spr = charSpr["rs"]
				if int(self.wset) == 1:
					self.spr = charSpr["rw1"]
				if int(self.wset) == 2:
					self.spr = charSpr["rw2"]
			if self.jumps == 1 or self.jumps == 3 or self.jumps == 5 or self.grav > 2:
				self.spr = charSpr["rf1"]
			if (self.jumps == 2 or self.jumps == 4) and self.grav < 2:
				self.spr = charSpr["rf2"]
		if self.direction == "left":
			if self.jumps == 0:
				# walk cycle set
				if self.walking == True:
					self.wset = self.wset + 0.1
					if self.wset > 3:
						self.wset = 1
				else:
					self.wset = 0
				if int(self.wset) == 0:
					self.spr = charSpr["ls"]
				if int(self.wset) == 1:
					self.spr = charSpr["lw1"]
				if int(self.wset) == 2:
					self.spr = charSpr["lw2"]
			if self.jumps == 1 or self.jumps == 3 or self.jumps == 5 or self.grav > 2:
				self.spr = charSpr["lf1"]
			if (self.jumps == 2 or self.jumps == 4) and self.grav < 1:
				self.spr = charSpr["lf2"]
# ibis class
class gibis:
	def __init__(self, name, onScreen, direction, spr, wset, spd, alive, health, x, y):
		self.alive = True
		self.name = name
		self.onScreen = False
		self.wset = 0
		self.health = 3
		self.spr = sprGibisStillR
		if randint(1,2) == 1:
			self.x = -33
			self.direction = "right"
		else:
			self.x = 833
			self.direction = "left"
		self.y = 536
		self.spd = randint(1,4) / 2
	def movements(self):
		if self.direction == "right":
			self.x = self.x + self.spd
			if self.onScreen == True and self.x >= 768:
				self.direction = "left"
		if self.direction == "left":
			self.x = self.x - self.spd
			if self.onScreen == True and self.x <= 0:
				self.direction = "right"
		if self.x >= 40 or self.x <= 768:
			self.onScreen = True
		if self.health <= 0:
			self.alive = False
	def sprSetGI(self):
		self.wset = self.wset + 0.05
		if self.wset >= 3:
			self.wset = 1
		if self.direction == "right":
			if int(self.wset) == 0:
				self.spr = sprGibisStillR
			if int(self.wset) == 1:
				self.spr = sprGibisWalk1R
			if int(self.wset) == 2:
				self.spr = sprGibisWalk2R
		if self.direction == "left":
			if int(self.wset) == 0:
				self.spr = sprGibisStillL
			if int(self.wset) == 1:
				self.spr = sprGibisWalk1L
			if int(self.wset) == 2:
				self.spr = sprGibisWalk2L
		screen.blit(self.spr, (self.x, 538))
class skibis:
	def __init__(self, name, onScreen, direction, spr, wset, spd, height, alive, x, y):
		self.alive = True
		self.name = name
		self.onScreen = False
		self.wset = 0
		self.spr = sprGibisStillR
		if randint(1,2) == 1:
			self.x = -33
			self.direction = "right"
		else:
			self.x = 833
			self.direction = "left"
		self.spd = randint(1,4) / 2
		self.y = 300
		self.height = randint(300,400)
	def movements(self):
		if self.direction == "right":
			self.x = self.x + self.spd
			if self.onScreen == True and self.x >= 768:
				self.direction = "left"
		if self.direction == "left":
			self.x = self.x - self.spd
			if self.onScreen == True and self.x <= 0:
				self.direction = "right"
		if self.x >= 40 or self.x <= 768:
			self.onScreen = True
		self.y = sin(self.x / 100) * 100 + self.height
	def sprSetGI(self):
		self.wset = self.wset + 0.01
		if self.wset >= 3:
			self.wset = 1
		if self.direction == "right":
			if int(self.wset) == 0:
				self.spr = sprSkibis1R
			if int(self.wset) == 1:
				self.spr = sprSkibis1R
			if int(self.wset) == 2:
				self.spr = sprSkibis2R
		if self.direction == "left":
			if int(self.wset) == 0:
				self.spr = sprSkibis1L
			if int(self.wset) == 1:
				self.spr = sprSkibis1L
			if int(self.wset) == 2:
				self.spr = sprSkibis2L
		screen.blit(self.spr, (self.x, self.y))
# ground draw
def groundDraw():
	for i in range (25):
		screen.blit(sprTile, (32*i,568))
# character select
def startup():
	global scene
	global selectedCharNum
	global started
	# arrow key inputs
	for event in pygame.event.get():
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_LEFT:
				selectedCharNum = selectedCharNum - 1
			if event.key == pygame.K_RIGHT:
				selectedCharNum = selectedCharNum + 1
			if event.key == pygame.K_DOWN:
				started = True
				scene = 2
		# quit game
		if event.type == pygame.QUIT:
			running = False
			pygame.quit()
			sys.exit()
	# character number boundaries
	if selectedCharNum >= 5:
		selectedCharNum = 4
	if selectedCharNum <= 0:
		selectedCharNum = 1
	# draws character icons onto the screen
	screen.blit(sprPigeonStillR, (329,279))
	if selectedCharNum == 1:
		screen.blit(sprPigeonSelected, (325,275))
	screen.blit(sprLordStillR, (379,279))
	if selectedCharNum == 2:
		screen.blit(sprLordSelected, (375,275))
	screen.blit(sprShreddedStillR, (429,279))
	if selectedCharNum == 3:
		screen.blit(sprShreddedSelected, (425,275))
	screen.blit(sprBearded, (479,275))
	if selectedCharNum == 4:
		screen.blit(sprBeardedSelected, (475,275))
	screen.blit(sprTitle, (325,150))
# decides if an ibis will spawn or not
def createIbis():
	if randint(1,550) == 1:
		if randint(1,4) == 1:
			skibisList.append(skibis("skibis_" + str(len(skibisList)), 1, 1, 1, 1, 1, 1, 1, 1, 1))
		else:
			gibisList.append(gibis("gibis_" + str(len(gibisList)), 1, 1, 1, 1, 1, 1, 1, 1, 1))
# main loop
gibisList.append(gibis("gibis_" + str(len(gibisList)), 1, 1, 1, 1, 1, 1, 1, 1, 1))
while running == True:
	# character select
	if scene == 1:
		screen.fill((255,255,255))
		startup()
	if playerInitialised == False and started == True:
		player = player(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
		player.alive = True
		playerInitialised = True
	# battle
	if scene == 2:
		screen.fill((173,216,230))
		groundDraw()
		player.platforming()
		player.sprSet()
		player.draw()
		createIbis()
		for i in range(len(gibisList)):
			if gibisList[i].alive == True:
				gibisList[i].movements()
				gibisList[i].sprSetGI()
		for j in range(len(gibisList)):
			if gibisList[j].alive == True:
				if player.x > gibisList[j].x - offset and player.x < gibisList[j].x + offset and player.y > gibisList[j].y - offset and player.y < gibisList[j].y + offset:
					player.grav = -3
					if player.y < gibisList[j].y - 22:
						gibisList[j].health = gibisList[j].health - player.dmg
						score = score + player.dmg
					else:
						running = False
		for q in range(len(skibisList)):
			if skibisList[q].alive == True:
				skibisList[q].movements()
				skibisList[q].sprSetGI()
		for s in range(len(skibisList)):
			if skibisList[s].alive == True:
				if player.x > skibisList[s].x - offset and player.x < skibisList[s].x + offset and player.y > skibisList[s].y - offset and player.y < skibisList[s].y + offset:
					player.grav = -3
					if player.y < skibisList[s].y - 22:
						skibisList[s].alive = False
						score = score + 6
					else:
						running = False
	pygame.display.update()
# You died window
window = tk.Tk()
window.title("Defeated!")
label = tk.Label(window, fg="black")
label.pack()
label.config(text=str("You died. Score: " + str(score)))
button = tk.Button(window, text="Okay", width=25, command=window.destroy)
button.pack()
window.mainloop()
pygame.quit()
sys.exit()
